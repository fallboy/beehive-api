var bcrypt = require('bcrypt-nodejs');

var username = process.argv[2];
var pw = process.argv[3];

var enc = bcrypt.hashSync(pw);

var user = {username: username, password: enc};

console.log(JSON.stringify(user));
