var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var itemSchema = new Schema({
  name: {type: String, required: false},
  imgUrl: {type: String, required: true},
  price: {type: Number, min: 0, default: 0},
  categoryId: {type: String, required: true},
  archived: {type: Boolean, default: false},
  paypalLink: {type: String},
  options: [String]
});


var categorySchema = new Schema({
  name: {type: String, required: true},
  caption: {type: String, required: true},
  order: {type: Number, required: true}
});

var message = new Schema({
  name: {type: String, required: true},
  email: {type: String, required: true},
  subject: {type: String, required: true},
  message: {type: String, required: true},
  messageDate: {type: Date, required: true},
  archived: {type: Boolean, required: false}
});

var event = new Schema({
  name: {type: String, required: true},
  eventDate: {type: Date, required: true},
  where: {type: String, required: true},
  archived: {type: Boolean, default: false}
});


var user = new Schema({
  username: {type: String, required: true},
  password: {type: String, required: true}
});


module.exports = {
  Item: mongoose.model('Item', itemSchema),
  Category: mongoose.model('Category', categorySchema),
  Message: mongoose.model('Message', message),
  Event: mongoose.model('Event', event),
  User: mongoose.model('User', user)
};