var mongoose = require('mongoose');
var logger = require('../util/logger');

var mongoURL= 'mongodb://127.0.0.1';


var connectDB = function(dburl) {
    mongoURL = dburl || mongoURL;
    logger.info('trying mongo connection : ',mongoURL);
    var options = {
        server: { socketOptions: { connectTimeoutMS: 2000 }},
        retryMilliSeconds: 5000,
        numberOfRetries: 100
    };
    mongoose.connect(mongoURL, options);
};


mongoose.connection.on('error', function(err){
    logger.error(err);
});

mongoose.connection.on('connected', function(){
    logger.info('mongo connected');
});


mongoose.connection.on('disconnected', function(){
    logger.error('mongo disconnected!');
});

module.exports = {
    connect : connectDB
};
