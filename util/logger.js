var bunyan = require('bunyan');
var PrettyStream = require('bunyan-prettystream');

var prettyOut = new PrettyStream();
prettyOut.pipe(process.stdout);


module.exports = bunyan.createLogger({
  name:'beehive-api'/*,
  streams: [
    {
      level: 'debug',
      type: 'raw',
      stream: prettyOut
    },
    {
      type: 'rotating-file',
      path: '/var/log/beehive-common.log',
      period: '7d',
      count: 4
    }
  ]*/
});