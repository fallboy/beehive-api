The OpenShift `nodejs` cartridge documentation can be found at:

http://openshift.github.io/documentation/oo_cartridge_guide.html#nodejs


---

Setting up for deployment.

1. Install rhc client tools [https://developers.openshift.com/en/getting-started-osx.html#client-tools](https://developers.openshift.com/en/getting-started-osx.html#client-tools)
2. `rhc setup` to login & negotiate keys
3. add mongo cartridge
4. set env variables
    * MONGO_SERVER_URL || OPENSHIFT_MONGODB_DB_URL
    * CORS_ALLOW_ORIGIN
    * MONGO_DB
    * JWT_SECRET
