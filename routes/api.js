var express = require('express');
var router = express.Router();
var models = require('../db/models');
var logger = require('../util/logger');
var Category = models.Category;
var Item = models.Item;
var Evt = models.Event;
var User = models.User;
var Message = models.Message;
var bcrypt = require('bcrypt-nodejs');
var jwt = require('jsonwebtoken');

function handleErr(err) {
  logger.error(err);
  var niceErr = new Error('API Error');
  niceErr.status = 500;

  return niceErr;
}


router.get('/categories', function (req, res, next) {

  Category.find({}).sort({order: 'asc'}).exec(function (err, cats) {
    if (err) {
      return next(handleErr(err));
    }
    res.send(cats);
  });

});

router.get('/items', function (req, res, next) {
  Item.find({archived: {$ne: true}}, function (err, items) {
    if(err) return next(handleErr(err));

    res.send(items);
  });
});


router.get('/categories/:categoryId/items', function (req, res, next) {
  var categoryId = req.params.categoryId;

  Item.find({categoryId: categoryId, archived: {$ne: true}}, function (err, items) {
    if (err) {
      return next(handleErr(err));
    }
    res.send(items);
  });
});


router.get('/events', function (req, res, next) {

  var today = new Date();

    Evt.find({archived: {$ne: true}, eventDate: {$gte: today}}).sort({eventDate: 'desc'}).exec(function (err, events) {
      if (err) return next(handleErr(err));

      res.send(events);
    });
});

router.put('/message', function (req, res, next) {
  var message = new Message(req.body);

  message.save(function (err, saved) {
    if (err) return next(handleErr(err));

    res.status(201).send(saved);
  });
});

var jwtSecret = process.env.JWT_SECRET;


router.post('/authenticate', function (req, res, next) {

  if(req.body.username && req.body.password){

    User.findOne({username:req.body.username}, function (err, user) {
        if(err) return next(handleErr(err));

        if(!user){
            return res.sendStatus(403);
        }

        var valid = bcrypt.compareSync(req.body.password, user.password);

        if(valid){
            var token = jwt.sign(user, jwtSecret, {
                expiresInMinutes: 7200 // expires in 5 days
            });
            res.status(202).send({token:token});
        }else {
            res.sendStatus(403);
        }

    });

  }else {
    next();
  }
});


router.use('/admin', require('./admin'));


module.exports = router;
