var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var models = require('../db/models');
var Category = models.Category;
var Item = models.Item;
var Message = models.Message;
var Evt = models.Event;
var logger = require('../util/logger');
var jwt = require('jsonwebtoken');


function handleErr(err) {
  if(err.errors){
    logger.error(err.errors)
  }else {
    logger.error(err);
  }

  var niceErr = new Error('API Error: ' + err.message);
  niceErr.status = 500;

  return niceErr;
}

router.use(function (req, res, next) {
  // check header or url parameters or post parameters for token
  var token = req.body.token || req.query.token || req.headers['x-access-token'];

  if (token) {

    var jwtSecret = process.env.JWT_SECRET;

    // verifies secret and checks exp
    jwt.verify(token, jwtSecret, function(err, decoded) {
      if (err) {
        return next(handleErr(err));
      } else {
        // if everything is good, save to request for use in other routes
        req.decoded = decoded;
        next();
      }
    });

  } else {

    // if there is no token
    // return an error
    return res.status(403).send({
      success: false,
      message: 'No token provided.'
    });

  }
});

router.put('/category', function (req, res, next) {

  var cat = new Category(req.body);

  cat.save(function (err, saved) {
    if (err) return next(handleErr(err));

    res.status(201).send(saved);
  });

});


router.put('/item', function (req, res, next) {
  var item = new Item(req.body);

  console.log('REQ.BODY === ', req.body);

  logger.debug({item:item}, 'Attempting to save new item');

  item.save(function (err, saved) {
    if (err) return next(handleErr(err));

    res.status(201).send(saved);
  });
});

router.get('/items/all', function (req, res, next) {
  Item.find({}, function (err, items) {
    if(err) return next(handleErr(err));

    res.send(items);
  });
});


router.delete('/item/delete/:id', function (req, res, next) {

  logger.info('Attempting to delete item with id: %s', req.params.id);

  try {
    //catch invalid ObjectId and make it a 404
    var id = new mongoose.Types.ObjectId(req.params.id);
  } catch (err) {
    return next();
  }

  Item.findById(req.params.id, function (err, item) {
    if (err) return next(handleErr(err));

    if (!item) {
      //send 404
      return next();
    }

    item.archived = true;
    item.save(function (err, item) {
      return res.sendStatus(202);
    });

  });
});


router.get('/messages', function (req, res, next) {

  Message.find({archived: {$ne: true}}).sort({messageDate: 'desc'}).exec(function (err, messages) {
    if(err) return next(handleErr(err));

    res.send(messages);
  });

});

router.delete('/message/delete/:id', function (req, res, next) {
  logger.info('Attempting to delete message with id :%s', req.params.id);

  try {
    //catch invalids and make it a 404
    var id = new mongoose.Types.ObjectId(req.params.id);
  }catch (err) {
    return next();
  }

  Message.findById(req.params.id, function (err, message) {
    if(err) return next(handleErr(err));

    if(!message){
      //send 404
      return next();
    }

    message.archived = true;
    message.save(function (err) {
      if(err) return next(handleErr(err));
      return res.sendStatus(202);

    });
  });
});



router.put('/event', function (req, res, next) {

  var evt = new Evt(req.body);

  evt.save(function (err, saved) {
    if (err) return next(handleErr(err));

    res.status(201).send(saved);
  });
});


router.get('/events/all', function (req, res, next) {
  Evt.find({archived: {$ne: true}}).sort({eventDate: 'desc'}).exec(function (err, events) {
    if(err) return next(handleErr(err));

    res.send(events);
  });
});


router.delete('/event/delete/:id', function (req, res, next) {

  logger.info('Attempting to delete event with id :%s', req.params.id);

  try {
    //catch invalids and make it a 404
    var id = new mongoose.Types.ObjectId(req.params.id);
  }catch (err) {
    return next();
  }

  Evt.findById(req.params.id, function (err, evt) {
    if(err) return next(handleErr(err));

    if(!evt){
      //send 404
      return next();
    }

    evt.archived = true;
    evt.save(function (err) {
      if(err) return next(handleErr(err));
      return res.sendStatus(202);

    });
  });
});


module.exports = router;