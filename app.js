var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cors = require('cors');
//var logger = require('express-bunyan-logger');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var Category = require('./db/models').Category;

var mongoServer = process.env.OPENSHIFT_MONGODB_DB_URL || process.env.MONGO_SERVER_URL || 'mongodb://127.0.0.1/';
var mongoDb = process.env.MONGO_DB || 'beehive';


require('./db/mongoConnector').connect(mongoServer + mongoDb);

var api = require('./routes/api');

var app = express();

// view engine setup
//app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
//app.use(require('express-bunyan-logger')());


var corsAllow = process.env.CORS_ALLOW_ORIGIN;
console.log('CORS ALLOW IS ', corsAllow);
if(corsAllow){
  app.use(cors({
    origin: corsAllow
  }));
  console.log('app should be using cors origin');
}else {
  app.use(cors());
}


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
//app.use(require('less-middleware')(path.join(__dirname, 'public')));
//app.use(express.static(path.join(__dirname, 'public')));


/**
 * Routes
 */
app.use('/api', api);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500)
    .send({
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500)
  .send('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
